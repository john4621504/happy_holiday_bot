"""app.py"""
import datetime
import numpy as np
import json
import scipy as sp
import random
import re
import itertools
import pandas as pd

from bybit_sdk import BybitClient
from telegram import Update, KeyboardButton, ReplyKeyboardMarkup, WebAppInfo
from telegram.ext import ApplicationBuilder, CallbackContext, CommandHandler, MessageHandler, filters
from credentials import BOT_TOKEN, BOT_USERNAME, WEBAPP_URL

def get_book_weighted_price(b_array, order_size):
    if len(b_array)==0:
        return None
    cum_size = np.cumsum(b_array[:, 1])
    valid_indices = np.where(cum_size >= order_size)[0]
    if len(valid_indices)==0:
        return None
    
    valid_index=valid_indices[0]
    if valid_index==0:
        order_book_weighted_price = b_array[0][0]
        return order_book_weighted_price
    else:
        total_value = np.sum(b_array[:valid_index, 0] * b_array[:valid_index, 1]) + \
                        b_array[valid_index, 0] * (order_size - cum_size[valid_index - 1])

    order_book_weighted_price = round(total_value / order_size,4)
    return order_book_weighted_price

def get_btcusdc_mid_price():
    api_client=BybitClient(None,None)
    btc_price_result=api_client.get_orderbook("spot","BTCUSDC")
    bid_p=float(btc_price_result['result']["b"][0][0])
    ask_p=float(btc_price_result['result']["a"][0][0])
    mid_p=(bid_p+ask_p)/2
    return round(mid_p,2)

def get_full_orderbook_real_price(symbol, sort_book_bid_dict):
    strike_price=option_info_dict[symbol]["strike_price"]
    b_array=sort_book_bid_dict[symbol]
    price_arr=b_array[:,0]
    size_arr=b_array[:,1]

    total_size=size_arr.sum()
    total_value=total_size*strike_price
    full_order_book_real_price=(price_arr*size_arr).sum()/total_size
    return full_order_book_real_price, total_size, total_value

def collect_option_info(remain_days_bound, edge_upper, edge_lower, op_filter):
    api_client=BybitClient(None,None)
    edge_price_range=range(edge_lower-1 , edge_upper+1)
    res=api_client.get_option_instruments_info("option", status="ONLINE", limit=1000)
    option_instruments_df=pd.DataFrame(res["result"]["list"])
    option_instruments_df=option_instruments_df[option_instruments_df['optionsType']=="Put"]

    option_instruments_df.loc[:,"deliveryTime"]=option_instruments_df["deliveryTime"].apply(lambda x:datetime.datetime.utcfromtimestamp(int(x)/1000))
    option_instruments_df.loc[:,"remain_days"]=(option_instruments_df["deliveryTime"]-datetime.datetime.utcnow())
    option_instruments_df.loc[:,"remain_days"]=option_instruments_df["remain_days"].apply(lambda x:x.total_seconds()/(60*60*24))

    option_instruments_df=option_instruments_df[option_instruments_df["remain_days"]<=remain_days_bound]
    option_instruments_df.loc[:,'strike_price']=option_instruments_df['symbol'].apply(lambda x:x.split("-")[-2]).astype(int)
    option_instruments_df=option_instruments_df[option_instruments_df["strike_price"].apply(lambda x:int(x) in edge_price_range)]

    option_instruments_df=option_instruments_df[["symbol","remain_days","strike_price"]].set_index("symbol")

    option_info_dict=option_instruments_df.T.to_dict()

    if op_filter!=None:
        filter_option_dict={}
        for key,value in option_info_dict.items():
            if op_filter in key:
                filter_option_dict[key]=value
        return filter_option_dict
    else:
        return option_info_dict

def get_option_book_price(option_info_dict):
    api_client=BybitClient(None,None)
    put_book_dict={}
    for symbol in option_info_dict.keys():
        print(f"Get {symbol} orderbook")
        put_result=api_client.get_orderbook("option", symbol, limit=25)['result']
        put_book_dict[symbol]=put_result

    sort_book_bid_dict={}
    for i in put_book_dict.keys():
        if len(put_book_dict[i]["b"])!=0:
            sort_book_bid_dict[i]=np.array(put_book_dict[i]["b"]).astype(float)
    print("Finish!")
    return sort_book_bid_dict
    
def calc_option_annually_return(option_info_dict, symbol, put_book_bid_dict, btc_price, calc_return_amount):
    api_client=BybitClient(None,None)

    strike_price=option_info_dict[symbol]["strike_price"]
    remain_days=option_info_dict[symbol]['remain_days']

    order_size=round(abs(calc_return_amount)/strike_price, 2)

    order_book_weighted_price=get_book_weighted_price(put_book_bid_dict[symbol], order_size)

    if order_book_weighted_price==None:
        return None
    ann_ret=((round(order_book_weighted_price,2)-btc_price*0.0002)/strike_price)/remain_days*365
    return round(ann_ret*100,2)


def get_all_annual_return(option_info_dict, put_book_bid_dict,calc_return_amount):
    btc_mid_p=get_btcusdc_mid_price()
    annual_return_dict={}

    print("Calculate Option return")
    for symbol in put_book_bid_dict.keys():
        annual_return=calc_option_annually_return(symbol, put_book_bid_dict, btc_mid_p, calc_return_amount)
        if annual_return!=None:
            annual_return_dict[symbol]=annual_return
    print("Calculate Finish")
    return annual_return_dict

def calc_port_weight_df(simulation_times, criteria, option_nums, annual_return_dict):
    
    option_count=len(annual_return_dict)
    
    if option_count<option_nums:
        assert False, "nums higher then op data length"
    
    qulified_weight_list=[]
    qulified_return_list=[]
    from tqdm.autonotebook import tqdm

    return_array=np.array(list(annual_return_dict.values()))
    for _ in tqdm(range(simulation_times)):
        weight_array=np.append(np.random.random(option_nums),np.zeros(option_count-option_nums))
        weight_array=((weight_array/sum(weight_array))*2).round(1)/2
        random.shuffle(weight_array)
        new_weight_array=np.array([0.05 if ((i<0.05 and i!=0)) else i for i in weight_array])
        diff_num=round(sum(new_weight_array)-1,2)
        new_weight_array[new_weight_array.argmax()]=new_weight_array.max()-diff_num

        port_return=(new_weight_array.dot(return_array)).round(2)
        if port_return>=criteria:
            if len(qulified_weight_list)==0:
                qulified_weight_list=np.array([new_weight_array])
            else:
                qulified_weight_list=np.vstack((qulified_weight_list,new_weight_array))

            if len(qulified_return_list)==0:
                qulified_return_list=np.array([port_return])
            else:
                qulified_return_list=np.append(qulified_return_list,port_return)

        weight_df=pd.DataFrame(qulified_weight_list,columns=annual_return_dict.keys())
        weight_df["port_return"]=qulified_return_list
        final_table=weight_df.sort_values(by="port_return", ascending=False).drop_duplicates()
    print(final_table)
    return final_table

def get_weight_list(option_nums, op_symbol_list):
    avg_invest_ratio=1/option_nums
    ratio_list = [0] * (len(op_symbol_list)-1)
    ratio_list.append(avg_invest_ratio)

    all_ratio_list=tuple(itertools.product(*[[0,avg_invest_ratio]]*len(op_symbol_list)))
    allow_weight_list=[]
    for i in all_ratio_list:
        if abs(sum(i)-1)<0.01:
            allow_weight_list.append(np.array(i))
    allow_weight_arr=np.array(allow_weight_list)
    return allow_weight_arr

def get_rank_weight_list(allow_weight_arr):
    rank_weight_arr=np.array([])
    for weight in allow_weight_arr.copy():
        index = np.arange(1, len(np.nonzero(weight)[0])+1, 1)
        weight[weight != 0] = index
        new_weight=weight/sum(weight)
        if len(rank_weight_arr)!=0:
            rank_weight_arr=np.vstack((rank_weight_arr,new_weight))
        else:
            rank_weight_arr=np.append(rank_weight_arr,new_weight)
    return rank_weight_arr

def get_invest_weight(option_info_dict, op_symbol_list, sort_book_bid_dict, btc_mid_p, port_invest_amount ,total_weight_arr):
    total_return_list=[]
    for weight_arr in total_weight_arr:
        port_return=0
        for op_symbol,weight in zip(op_symbol_list,weight_arr):
            invest_amount=port_invest_amount*weight
            real_annually_return=calc_option_annually_return(option_info_dict, op_symbol, sort_book_bid_dict,
                                                          btc_mid_p, invest_amount)
            if real_annually_return==None:
                break
            port_return+=real_annually_return*weight
        if real_annually_return!=None:
            total_return_list.append(port_return)
        else:
            total_return_list.append(None)
    weight_df=pd.DataFrame(total_weight_arr,columns=op_symbol_list)
    # weight_df["port_return"]=[round(i,4) for i in total_return_list]
    weight_df["port_return"] = [round(i, 4) if i is not None else None for i in total_return_list]
    weight_df=weight_df.sort_values(by="port_return")
    return weight_df

def get_invest_weight_arr(option_nums, op_symbol_list, weight_type=1):
    """
    weight type 1 avg invest all options
    weight type 2 invest all options by rank

    type 2 example

    strike price 20000,   19000,    18000
    rank         1,       2,        3
    weight      1/(1+2+3) 2/(1+2+3) 3/(1+2+3)
    """
    allow_weight_arr = get_weight_list(option_nums, op_symbol_list)
    if weight_type==1:
        invest_weight_arr = allow_weight_arr.copy()
    elif weight_type==2:
        invest_weight_arr = get_rank_weight_list(allow_weight_arr)
    return invest_weight_arr

def output_put_dict(weight_df, criteria, index, port_invest_amount):
    output_weight_ser=weight_df[(weight_df["port_return"]>criteria[0]) & (weight_df["port_return"]<criteria[1])].loc[index]
    print("Target Output Weight")
    print_d=output_weight_ser.replace(0,None).dropna().to_dict()
    print(print_d)
    output_dict=(output_weight_ser[output_weight_ser!=0]*port_invest_amount).to_dict()
    temp=output_dict.pop("port_return")

    tidy_output_dict={}
    for k,v in output_dict.items():
        tidy_output_dict[int(k.split("-")[-2])]=-round(v,2)

    print("Output rader_output_put_dict.json")
    with open("rader_output_put_dict.json","w") as file:
        json.dump(tidy_output_dict,file,indent=2)
    print("Finish!")

def print_weight_func(weight_df, criteria, print_lines):
    print_weight_df=weight_df[(weight_df["port_return"]>criteria[0]) & (weight_df["port_return"]<criteria[1])]
    # 确保打印的行数不超过DataFrame的大小
    print_lines = min(print_lines, len(print_weight_df))
    result = []
    for i in range(print_lines):
        index=print_weight_df.index[i]
        print_d=print_weight_df.iloc[i].replace(0,None).dropna().to_dict()
        total_print_d={index:print_d}
        result.append(total_print_d)
    return result

async def launch_web_ui(update: Update, callback: CallbackContext):
    # display our web-app!
    kb = [
        [KeyboardButton(
            "Caculate my option positions!", 
           web_app=WebAppInfo(WEBAPP_URL)
        )]
    ]
    await update.message.reply_text("Let's do this...", reply_markup=ReplyKeyboardMarkup(kb))

async def web_app_data(update: Update, context: CallbackContext):
    data = json.loads(update.message.web_app_data.data)
    reply_data = []
    for result in data:
        reply_data.append(f"{result['name']}: {result['value']}")
    
    #I have op_filter, port_invest_amount, option_nums, weight_type, low_bound, high_bound in reply_data please find them
    # and convert option_nums to integer, weight_type to integer, low_bound and high_bound to float
    # and then call get_option_positions(op_filter, port_invest_amount, option_nums, weight_type, low_bound, high_bound)
    op_filter=None
    port_invest_amount=None
    option_nums=None
    weight_type=None
    low_bound=None
    high_bound=None
    for result in data:
        if result['name'] == 'op_filter':
            op_filter = result['value']
        elif result['name'] == 'port_invest_amount':
            port_invest_amount = float(result['value'])
        elif result['name'] == 'option_nums':
            option_nums = int(result['value'])
        elif result['name'] == 'weight_type':
            weight_type = int(result['value'])
        elif result['name'] == 'low_bound':
            low_bound = float(result['value'])
        elif result['name'] == 'high_bound':
            high_bound = float(result['value'])
    positions = get_option_positions(op_filter, port_invest_amount, option_nums, weight_type, low_bound, high_bound)

    reply_data.append("\n--------------------\n")
    reply_data.append("\nPossible positions portfolio:\n")
    for position in positions:
        reply_data.append(str(position) + "\n")

    await update.message.reply_text("Your criteria was:\n" + "\n".join(reply_data))

def get_option_positions(op_filter: str, port_invest_amount: float, option_nums: int, weight_type: int, low_bound: float, high_bound: float):
    #Get BTC & option data
    btc_mid_p=get_btcusdc_mid_price()
    remain_days_bound=10
    edge_upper=int(btc_mid_p) #option strike price upper bound (should lower than btc price)
    edge_lower=int(btc_mid_p*0.7) #option strike price lower bound
    # while True:
    #     op_filter = input("Enter option date symbol (e.g. 5JAN24): ")
    #     if re.match(r"\d{1,2}(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)\d{2}$", op_filter):
    #         break
    #     else:
    #         print("Invalid input. Please enter a valid option date symbol.")

    # Use op_filter in your code

    option_info_dict = collect_option_info(remain_days_bound, edge_upper, edge_lower, op_filter)
    sort_book_bid_dict = get_option_book_price(option_info_dict)
    op_symbol_list=list(sort_book_bid_dict.keys())
    

    #%%
    # while True:
    #     try:
    #         port_invest_amount = float(input("Enter investment amount (USD): "))
    #         option_nums = int(input("Enter the number of parts to separate the investment amount (default is 2): ") or 2)

    #         weight_type = int(input("Enter weight allocation type (1 for By average, 2 for By rank): "))
    #         if weight_type != 1 and weight_type != 2:
    #             raise ValueError("Invalid input. Please enter either 1 or 2.")
    #         break
    #     except ValueError as e:
    #         print(e)

    # if weight_type == 1:
    #     print(f"Invest {port_invest_amount} USD separated to {option_nums} parts by average")
    # else:
    #     print(f"Invest {port_invest_amount} USD separated to {option_nums} parts by rank")
        
    invest_weight_arr = get_invest_weight_arr(option_nums, op_symbol_list, weight_type)

    #Get weight_df
    weight_df = get_invest_weight(option_info_dict, op_symbol_list, sort_book_bid_dict, btc_mid_p, port_invest_amount ,invest_weight_arr)
    print(weight_df)

    #Print weight
    print_lines=50

    # while True:
    #         low_bound = float(input("Enter profit percent low bound: "))
    #         high_bound = float(input("Enter profit percent high bound: "))
    criteria = [low_bound, high_bound]
    print("L:", str(criteria[0]) + "%", "H:", str(criteria[1]) + "%")
    result = print_weight_func(weight_df, criteria, print_lines)
    print(result)

    return result

    #         save_output = input("Do you want to save the output? (Y/N): ")
    #         if save_output.upper() == "Y":
    #             index = int(input("Choose Output Index: "))
    #             output_put_dict(weight_df, criteria, index, port_invest_amount)
    #             break


if __name__ == '__main__':
    # when we run the script we want to first create the bot from the token:
    application = ApplicationBuilder().token(BOT_TOKEN).build()

    # and let's set a command listener for /start to trigger our Web UI
    application.add_handler(CommandHandler('start', launch_web_ui))

    # as well as a web-app listener for the user-inputted data
    application.add_handler(MessageHandler(filters.StatusUpdate.WEB_APP_DATA, web_app_data))

    # and send the bot on its way!
    print(f"Your bot is listening! Navigate to http://t.me/{BOT_USERNAME} to interact with it!")
    application.run_polling()