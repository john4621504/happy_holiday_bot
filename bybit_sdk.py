import requests
import time
import hmac
import hashlib
import json
from urllib.parse import urljoin, urlencode
class BybitClient:
    def __init__(self, api_key=None, api_secret=None):
        self.api_key = api_key
        self.api_secret = api_secret
        self.base_url = "https://api.bybit.com"

    def _remove_none_params(self, raw_params):
        params = {}
        for k, v in raw_params.items():
            if v is not None:
                params[k] = v

        return params
    
    def genSignature(self, payload, request_type):
        if request_type=="GET":
            query_string = urlencode(payload)
        elif request_type=="POST":
            query_string= json.dumps(payload)
        time_stamp=str(int(time.time() * 10 ** 3))
        param_str= time_stamp + self.api_key + str(5000) + query_string
        hash = hmac.new(self.api_secret.encode("utf-8"), param_str.encode("utf-8"),hashlib.sha256)
        signature = hash.hexdigest()
        return signature

    def getHeader(self, payload, request_type="GET"):

        time_stamp=str(int(time.time() * 10 ** 3))
        signature=self.genSignature(payload, request_type)
        headers = {
            "Content-Type": "application/json",
            "X-BAPI-API-KEY": self.api_key,
            "X-BAPI-SIGN": signature,
            "X-BAPI-SIGN-TYPE" : "2",
            "X-BAPI-TIMESTAMP": time_stamp,
            "X-BAPI-RECV-WINDOW": str(5000)}
        return headers

    def _requests_private_post(self, url, payload={}):
        headers=self.getHeader(payload, "POST")

        response = requests.post(url, json=payload, headers=headers)
        return response
    
    def _requests_private_get(self, url, payload={}):
        headers=self.getHeader(payload, "GET")

        response = requests.get(url, params=payload, headers=headers)
        return response

    def _requests_public_get(self, url, payload={}):
        
        url=url+"?"+urlencode(payload)
        response = requests.get(url)
        return response

    def place_order(self, category, symbol, side, qty, price, orderLinkId, timeInForce="GTC", order_type="Market"):
        """
        timeInForce : GTC, IOC, FOK, PostOnly, default GTC
        order_type : Market, Limit
        category : spot, linear, option (for unified account)
        """
        endpoint = "/v5/order/create"
        url = self.base_url + endpoint
        payload = {
            "category": category,
            "symbol": symbol,
            "side": side,
            "orderType": order_type,
            "qty": qty,
            "price": price,
            "timeInForce": timeInForce,
            "orderLinkId": orderLinkId
        }
        
        new_payload=self._remove_none_params(payload)

        response=self._requests_private_post(url, new_payload)

        return response.json()

    def cancel_order(self, category, symbol, orderLinkId):

        endpoint = "/v5/order/cancel"
        url = self.base_url + endpoint
        payload = {
            "category": category,
            "symbol": symbol,
            "orderLinkId": orderLinkId
        }
        response=self._requests_private_post(url, payload)

        return response.json()

    def get_working_order(self, category, symbol):
        endpoint = "/v5/order/realtime"
        url = self.base_url + endpoint
        payload = {
            "category": category,
            "symbol": symbol
        }

        response=self._requests_private_get(url, payload)

        return response.json()



    def check_position(self, category):
        endpoint = "/v5/position/list"
        url = self.base_url + endpoint
        payload = {
            "category":category
        }

        response=self._requests_private_get(url, payload)

        return response.json()


    def wallet_balance(self, accountType, coin=None):
        endpoint = "/v5/account/wallet-balance"
        url = self.base_url + endpoint
        payload={
                    "accountType":accountType,
                    "coin":coin
                }

        new_payload=self._remove_none_params(payload)
        response=self._requests_private_get(url, new_payload)

        return response.json()


    
    def query_account_coins_balance(self, accountType):
        endpoint = "/v5/asset/transfer/query-account-coins-balance"
        url = self.base_url + endpoint
        payload={
                    "accountType":accountType
                }
        response=self._requests_private_get(url, payload)

        return response.json()



    def account_info(self):
        endpoint = "/v5/account/info"
        url = self.base_url + endpoint
        
        response=self._requests_private_get(url)

        return response.json()



    def check_asset_info(self):
        endpoint = "/v5/asset/transfer/query-asset-info"
        url = self.base_url + endpoint


        response=self._requests_private_get(url)

        return response.json()

    def get_orderbook(self, category, symbol, limit=1):

        endpoint = "/v5/market/orderbook"
        url = self.base_url + endpoint

        payload={
                    "category":category,
                    "symbol":symbol,
                    "limit":limit
                }

        response=self._requests_public_get(url, payload)

        return response.json()
    
    def get_tickers(self, category, symbol, baseCoin=None, expDate=None):

        endpoint = "/v5/market/tickers"
        url = self.base_url + endpoint

        payload={
                    "category":category,
                    "symbol":symbol,
                    "baseCoin":baseCoin,
                    "expDate":expDate
                }

        new_payload=self._remove_none_params(payload)
        response=self._requests_public_get(url, payload)

        return response.json()
    
    def get_option_instruments_info(self, category, status="ONLINE", limit=500):

        endpoint = "/v5/market/instruments-info"
        url = self.base_url + endpoint

        payload={
                    "category":category,
                    "status":status,
                    "limit":str(limit)
                }

        response=self._requests_public_get(url, payload)

        return response.json()

    def get_option_implied_vol(self, category="option", baseCoin=None, period=None, startTime=None, endTime=None):
        
        endpoint = "/v5/market/historical-volatility"
        url = self.base_url + endpoint

        payload={
                    "category":category,
                    "baseCoin":baseCoin,
                    "period":period,
                    "startTime":startTime,
                    "endTime":endTime
                }
        
        new_payload=self._remove_none_params(payload)
        response=self._requests_public_get(url, new_payload)

        return response.json()